import numpy as np
import sys
import os
import datetime

userDict = {}
moviesDict = {}
userWeights = {}

def readFile(filename):
    arr = np.loadtxt(os.getcwd() + '/' + filename, dtype='int, int, float', delimiter=',')
    return arr

def populateUserData(data):
    for item in data:
        if userDict.has_key(item[1]):
            userDict[item[1]]['mean_vote'] += item[2]
            userDict[item[1]]['movies'][item[0]] = item[2]
        else:
            userDict[item[1]] = {}
            userDict[item[1]]['mean_vote'] = item[2]
            userDict[item[1]]['movies'] = {}
            userDict[item[1]]['movies'][item[0]] = item[2]
            userWeights[item[1]] = {}
        if moviesDict.has_key(item[0]):
            moviesDict[item[0]][item[1]] = item[2]
        else:
            moviesDict[item[0]] = {}
            moviesDict[item[0]][item[1]] = item[2]
    for key in userDict.iterkeys():
        length = len(userDict[key]['movies'])
        if length > 0:
            meanVote = userDict[key]['mean_vote'] / length
            userDict[key]['mean_vote'] = int(meanVote * 1000)

def calculateWeights():
    print 'S ', datetime.datetime.now()
    for activeUser in userDict.iterkeys():
        for key in userDict.iterkeys():
            if activeUser != key and userWeights[key].has_key(activeUser) == False:
                activeUserMovies = userDict[activeUser]['movies']
                otherUserMovies = userDict[key]['movies']
                movies = activeUserMovies
                if(len(activeUserMovies) > len(otherUserMovies)):
                    movies = otherUserMovies
                numerator = 0
                denominator1 = 0
                denominator2 = 0
                au_mean_vote = userDict[activeUser]['mean_vote']
                ou_mean_vote = userDict[key]['mean_vote']
                for movie in movies:
                    if activeUserMovies.has_key(movie) and otherUserMovies.has_key(movie):
                        au_error = activeUserMovies[movie] - au_mean_vote
                        ou_error = otherUserMovies[movie] - ou_mean_vote
                        numerator += (au_error * ou_error)
                        denominator1 += (au_error ** 2)
                        denominator2 += (ou_error ** 2)
                denominator = (denominator1 * denominator2) ** 0.5
                #if numerator != 0 and denominator != 0:
                    #userWeights[activeUser][key] = int((numerator / denominator) * 1000)
                    #userWeights[key][activeUser] = userWeights[activeUser][key]
    print 'F ', datetime.datetime.now()

def test(filename):
    testdataset = readFile(filename)
    mae = 0.0
    rms = 0.0
    for item in testdataset:
        if moviesDict.has_key(item[0]) and userWeights.has_key(item[1]):
            movieUsers = moviesDict[item[0]]
            userWeight = userWeights[item[1]]
            predictedRating = userDict[item[1]]['mean_vote']
            kappa = 1.0 / sum(userWeight.values())
            for user, rating in movieUsers.iteritems():
                weight = 0
                if userWeight.has_key(user):
                    weight = userWeight[user]
                predictedRating += (kappa * (weight / 1000.0) * (rating - userDict[user]['mean_vote']))
            mae += abs(predictedRating - item[2])
            rms += ((predictedRating - item[2]) ** 2)
    mae = mae / len(testdataset)
    rms = (rms / len(testdataset)) ** 0.5
    print mae, rms

def main(args):
    dataset = readFile(args[1])
    populateUserData(dataset)
    print len(userWeights)
    calculateWeights()
    #test(args[2])

main(sys.argv)
